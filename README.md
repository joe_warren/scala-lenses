# Scala-Lenses

This is a WIP optics library in Scala. 

It's 100% a learning exercise, so _please_ don't try and use this in anything. 

If you actually want to use Optics in your Scala Code, I recommend using [Monocle by Julien Truffaut.](https://github.com/julien-truffaut/Monocle)

## Sources

[van Laarhoven Lenses](https://www.twanvl.nl/blog/haskell/cps-functional-references)
[Traversals](https://www.cs.ox.ac.uk/jeremy.gibbons/publications/iterator.pdf)
