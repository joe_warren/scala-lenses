package it.doscienceto.lens

import it.doscienceto.lens.typeclasses.instances._
import it.doscienceto.lens.concrete.{BinaryTree, Bitwise, Eithers, Lists, Maps, NonEmptyTree, Tuples}
import org.scalatest.{FlatSpec, Matchers}

import scala.None

class TraversalSpec extends FlatSpec with Matchers {

  val tree = BinaryTree.Branch(
    BinaryTree.Leaf(),
    1,
    BinaryTree.Branch(
      BinaryTree.Leaf(),
      2,
      BinaryTree.Leaf()
    )
  )
  val nonEmptyTree = NonEmptyTree.Branch(NonEmptyTree.Leaf(1),
    NonEmptyTree.Branch(
      NonEmptyTree.Leaf(2),
      NonEmptyTree.Leaf(3)
    )
  )
  val map = Map(
    "one" -> 1,
    "two" -> 2,
    "three" -> 3
  )

  "Traverals" should "aggregate monoidal values (via getters)" in {
    import Getter._
    val t1 = Maps.values[String, Int, Sum].mapOf{Sum(_)}(map)
    val t = Maps.values[String, Sum, Sum].get(t1)
    t.value should be(6)
  }
  it should "foldOf over values" in {
    val t = Maps.values.foldOf{ x: Int => y: Int => x+y }(0)(map)
    t should be(6)
  }
  it should "let you coerce traversable objects to a List" in {
    val t = Maps.values.buildListOf(map)
    t should contain theSameElementsAs (List(1, 2, 3))

    val list = BinaryTree.traversal.buildListOf(tree)
    list should be(List(1, 2))
  }
  it should "let you map over values" in {
    val t = Maps.values.mapOf{i:Int => (i*2).toString()}(map)
    t should be(Map(
      "one" -> "2",
      "two" -> "4",
      "three" -> "6"
    ))
    val t2 = BinaryTree.traversal.mapOf{ i: Int => (i* 2).toString }(tree)
    t2 should be( BinaryTree.Branch(
      BinaryTree.Leaf(),
      "2",
      BinaryTree.Branch(
        BinaryTree.Leaf(),
        "4",
        BinaryTree.Leaf()
      )
    ))
  }
  it should ("support traversing directly") in {
    val r = BinaryTree.traversal[Int, Unit].apply({case x: Int => Writer(List(x), ())}).apply(tree)
    r.written should be(List(1, 2))
  }
  it should ("support traversing in reverse") in {
    val r = BinaryTree.traversal[Int, Int].reverse.buildListOf(tree)
    r should be(List(2, 1))
  }
  it should "compose with other traversals" in {
    val treeOfLists = NonEmptyTree.traversal[Int, List[String]].mapOf{ i => List.fill(i)("A")}(nonEmptyTree)
    val composed: Traversal[NonEmptyTree.NonEmptyTree[List[String]], NonEmptyTree.NonEmptyTree[List[String]], String, String] = Traversal.compose(
      NonEmptyTree.traversal[List[String], List[String]],
      Lists.traversal[String, String]
    )
    val t = composed.mapOf{x => x.toLowerCase}(treeOfLists)
    t should be (
      NonEmptyTree.Branch(NonEmptyTree.Leaf(List("a")),
        NonEmptyTree.Branch(
          NonEmptyTree.Leaf(List("a", "a")),
          NonEmptyTree.Leaf(List("a", "a", "a"))
        )
      )
    )
  }

  it should "compose with lenses, forming a traversal" in {
    val treeOfTuples = NonEmptyTree.traversal[Int, (Int, Int)].mapOf { i => (i, i) }(nonEmptyTree)
    val src = ("nonEmptyTree", treeOfTuples)
    type Src1 = NonEmptyTree.NonEmptyTree[(Int, Int)]
    type Dst1 = NonEmptyTree.NonEmptyTree[(String, Int)]
    val composed: Traversal[(String, Src1), (String, Dst1), Int, String] =
      Traversal.compose[Int, String, Src1, Dst1, (String, Src1), (String, Dst1)](
        Tuples.second[String, NonEmptyTree.NonEmptyTree[(Int, Int)], NonEmptyTree.NonEmptyTree[(String, Int)]],
        Traversal.compose[Int, String, (Int, Int), (String, Int), Src1, Dst1](
          NonEmptyTree.traversal[(Int, Int), (String, Int)],
          Tuples.first[Int, String, Int]
        )
      )
    val result = composed.mapOf{i:Int => i.toString}(src)
    result should be (("nonEmptyTree",
      NonEmptyTree.Branch(NonEmptyTree.Leaf(("1", 1)),
        NonEmptyTree.Branch(
          NonEmptyTree.Leaf(("2", 2)),
          NonEmptyTree.Leaf(("3", 3))
        )
      )
    ))
  }
  it should "as a getter (with a monoidal target), compose with lenses, to form a getter" in {
    val treeOfSums = NonEmptyTree.traversal[Int, Sum].mapOf{Sum(_)}(nonEmptyTree)
    val traversal = NonEmptyTree.traversal[Sum, Sum]
    val sumLens = Lens.simpleLens[Sum, Int](
      {case Sum(i) => i},
      {case _ => {case i => Sum(i)}}
    )
    import Getter._
    val v = compose(traversal, sumLens).get(treeOfSums)
    v should be(6)
  }
  it should "support creating a new traversal, taking or dropping n targets, when monomorphic" in {
    val taking3 = Lists.traversal[Int, Int]().taking(3)
    val dropping3 = Lists.traversal[Int, Int]().dropping(3)
    taking3.mapOf(_*(-1))((1 to 5).toList) should be(List(-1, -2, -3, 4, 5))
    dropping3.mapOf(_*(-1))((1 to 5).toList) should be(List(1, 2, 3, -4, -5))
  }
  it should "support creating an optional, over the head of the items, when monomorphic" in {
    val optional = Lists.traversal[Int, Int].headOptional
    optional.getOption(List(1, 2, 3)) should be(Some(1))
    optional.getOption(List.empty[Int]) should be(None)
    optional.mapOf(_*2)(List(1, 2, 3)) should be (List(2, 2, 3))
  }
  "bits traversals" should "traverse over the bits in an int" in {
    Bitwise.bits.buildListOf(7) should be (List(true, true, true) ++ List.fill(29)(false))
    Bitwise.bits.mapOf(!_)(23412) should be(~23412) // ~23412 == -23413
    Bitwise.bits.taking(3).mapOf(_ => true)(0) should be(7) // 111b = 7
  }
  "traversals" should "compose with prisms" in {
    val t = Lists.traversal[Either[Int, String], Either[Boolean, String]]
    val p = Eithers.leftPrism[Int, Boolean, String]
    val tp = Traversal.compose(t, p)
    val list = List(Left(0), Right("foo"), Left(10), Right("bar"))
    tp.mapOf(_ > 5)(list) should be (
      List(Left(false), Right("foo"), Left(true), Right("bar"))
    )
  }
}
