package it.doscienceto.lens
import it.doscienceto.lens.concrete.Lists
import org.scalatest.{FlatSpec, Matchers}

class IsoSpec extends FlatSpec with Matchers {
  import Isomorphism._

  val listStringIso = simpleIso[List[Char], String] (
    _.mkString(""),
    _.toList
  )

  "Isomorphism" should "Support viewing" in {
    listStringIso.view(List('a', 'b', 'c')) should be ("abc")
  }
  it should "support transformation over" in {
    listStringIso.over(_.toUpperCase)(List('a', 'b', 'c')) should be(List('A', 'B', 'C'))
  }
  it should "support inverting" in {
    listStringIso.inverse.view("abc") should be(List('a', 'b', 'c'))
  }
  it should "support composing with itself" in {
    case class Wrapper1(value: String)
    case class Wrapper2(x: String)
    val iso1 = simpleIso[Wrapper1, String](_.value, Wrapper1)
    val iso2 = simpleIso[Wrapper2, String](_.x, Wrapper2)
    val combinedIso = compose(iso1, iso2.inverse)
    combinedIso.view(Wrapper1("hello, world")) should be (Wrapper2("hello, world"))
  }
  it should "compose with traversals" in {
    import Traversal._
    val t: Traversal[String, String, Char, Char]
      = compose[Char, Char, List[Char], List[Char], String, String](listStringIso.inverse, Lists.traversal[Char, Char]())
    t.mapOf(_.toTitleCase)("abc") should be("ABC")
  }

}
