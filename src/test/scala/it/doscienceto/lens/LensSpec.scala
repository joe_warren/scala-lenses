package it.doscienceto.lens

import java.time.LocalDate

import it.doscienceto.lens.concrete.{At, Bitwise, Eithers, Tuples}
import org.scalatest._

class LensSpec extends FlatSpec with Matchers {
  case class Person(_name: Name, _born: Born, _address: Address)
  case class Name(_foreNames: String /*Space separated*/ , _surName: String)
  type EpochDay = Long
  case class Born(_bornAt: Address, _bornOn: EpochDay)
  case class Address(_street: String, _houseNumber: Int,
                     _place: String /*Village / city*/ , _country: String)

  case class Gregorian(_year: Int, _month: Int, _dayOfMonth: Int)


  val personBornLens = Lens.simpleLens[Person, Born](_._born, p => b => p.copy(_born = b))
  val bornDayLens = Lens.simpleLens[Born, EpochDay](_._bornOn, b => d => b.copy(_bornOn = d))
  val dayMonthLens = Lens.simpleLens[EpochDay, Int](LocalDate.ofEpochDay(_).getMonthValue, ed => m => LocalDate.ofEpochDay(ed).withMonth(m).toEpochDay)
  val personAddressLens = Lens.simpleLens[Person, Address](_._address, p => a => p.copy(_address = a))
  val bornAddressLens = Lens.simpleLens[Born, Address](_._bornAt, b => a => b.copy(_bornAt = a))
  val addressStreetLens = Lens.simpleLens[Address, String](_._street, a => s => a.copy(_street = s))

  val bornStreet: Born => String = Lens.compose(bornAddressLens, addressStreetLens).get(_)

  val setCurrentStreet: String => Person => Person = s => p => Lens.compose(personAddressLens, addressStreetLens).set(p, s)

  val setBirthMonth: Int => Person => Person = m => p => Lens.compose(Lens.compose(personBornLens, bornDayLens), dayMonthLens).set(p, m)

  // Transform both birth and current street names.
  val renameStreets: (String => String) => Person => Person = fn =>
    Lens.compose(personAddressLens, addressStreetLens).modify(fn).compose(
      Lens.compose(Lens.compose(personBornLens, bornAddressLens), addressStreetLens).modify(fn)
    )

  def toEpochDay(year: Int, month: Int, dayOfMonth: Int): Long=
    LocalDate.of(year, month, dayOfMonth).toEpochDay

  val testPerson: Person =
    Person(
      _name = Name(
        _foreNames = "Jane Joanna",
        _surName = "Doe"),
      _born = Born(
        _bornAt = Address(
          _street = "Longway",
          _houseNumber = 1024,
          _place = "Springfield",
          _country = "United States"),
        _bornOn = toEpochDay(1984, 4, 12)),
      _address = Address(
        _street = "Shortlane",
        _houseNumber = 2,
        _place = "Fallmeadow",
        _country = "Canada"))

  "bornStreet" should "work" in {
    bornStreet(testPerson._born) should be("Longway")
  }
  "setCurrentStreet" should "work" in {
    (setCurrentStreet("Middleroad")(testPerson))._address._street should be ("Middleroad")
  }
  "setBirthMonth" should "work" in {
    setBirthMonth(9)(testPerson)._born._bornOn should be (toEpochDay(1984, 9, 12))
  }
  "renameStreets" should "work" in {
    renameStreets(_.toUpperCase)(testPerson)._address._street should be ("SHORTLANE")
    renameStreets(_.toUpperCase)(testPerson)._born._bornAt._street should be ("LONGWAY")
  }
  "non monomorphic lenses" should "work" in {
    val a = (1, "world")
    val b = Tuples.first.modify((_: Int).toString)(a)
    b should be("1", "world")
  }
  "At" should "focus on a key in a map" in {
    val m = Map(1 -> "hello", 2 -> "world")
    type M = Map[Int, String]
    At.at[M, Int, String](2).modify(_.map(_.toUpperCase))(m) should be(Map(1 -> "hello", 2 -> "WORLD"))
    At.at[M, Int, String](3).set(m, Some("again")) should be(Map(1 -> "hello", 2 -> "world", 3->"again"))
    At.at[M, Int, String](1).set(m, None) should be(Map(2 -> "world"))
    At.at[M, Int, String](1).get(m) should be(Some("hello"))
  }
  "Eithers.bothLens" should "focus on the active side of an Either" in {
    Eithers.bothLens.get(Left(1)) should be(1)
    Eithers.bothLens[Int, Int].modify(_*2)(Right(2)) should be(Right(4))
    Eithers.bothLens[Int, String].set(Right(2), "hello, world") should be(Right("hello, world"))
  }
  "Bitwise.bitAt" should "focus on one bit in an int" in {
    Bitwise.bitAt(1).set(0, true) should be(2)
    Bitwise.bitAt(0).get(3) should be(true)
    Bitwise.bitAt(0).get(4) should be(false)
    Bitwise.bitAt(2).modify(!_)(4) should be(0)
    Bitwise.bitAt(2).modify(!_)(3) should be(7)

  }
}
