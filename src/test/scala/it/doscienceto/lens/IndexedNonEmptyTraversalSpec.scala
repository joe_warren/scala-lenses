package it.doscienceto.lens
import it.doscienceto.lens.concrete.NonEmptyTree
import org.scalatest.{FlatSpec, Matchers}

class IndexedNonEmptyTraversalSpec extends FlatSpec with Matchers {
  val tree: NonEmptyTree.NonEmptyTree[String] =
    NonEmptyTree.Branch(
      NonEmptyTree.Branch(
        NonEmptyTree.Branch(
         NonEmptyTree.Leaf("third layer"),
          NonEmptyTree.Leaf("also third")
        ),
        NonEmptyTree.Leaf("second layer")
      ),
      NonEmptyTree.Leaf("first layer")
  )


  "NonEmptyTree.traversal" should "support indexing with a path" in {
    def printPath(path: NonEmptyTree.Index): String = path.map{
      case NonEmptyTree.L => "L"
      case NonEmptyTree.R => "R"
    }.mkString(",")

    val result = NonEmptyTree.traversal[String, String].indexedMapOf{
      case (path, value) => s"${printPath(path)}:$value"
    }(tree)
    result should be(
      NonEmptyTree.Branch(
        NonEmptyTree.Branch(
          NonEmptyTree.Branch(
            NonEmptyTree.Leaf("L,L,L:third layer"),
            NonEmptyTree.Leaf("L,L,R:also third")
          ),
          NonEmptyTree.Leaf("L,R:second layer")
        ),
        NonEmptyTree.Leaf("R:first layer")
      )
    )
  }
  it should "support traversing over a particular layer of the tree" in {
    NonEmptyTree.layer(3).buildListOf(tree) should be(
      List("third layer", "also third")
    )
    NonEmptyTree.layer(0).buildListOf(NonEmptyTree.Leaf[Int](1)) should be(List(1))
  }

  it should "support mapping over the index" in {
    NonEmptyTree.traversal.mapIndex(_.length).indexedMapOf{ (i: Int, _: String) => i }(tree) should be(
    NonEmptyTree.Branch(
      NonEmptyTree.Branch(
        NonEmptyTree.Branch(
          NonEmptyTree.Leaf(3),
          NonEmptyTree.Leaf(3)
        ),
        NonEmptyTree.Leaf(2)
      ),
      NonEmptyTree.Leaf(1)
    )
    )
    NonEmptyTree.traversal.mapIndex(_.length).headOf(tree) should be("third layer")
  }


}
