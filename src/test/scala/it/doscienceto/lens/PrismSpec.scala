package it.doscienceto.lens

import it.doscienceto.lens.concrete.{Eithers, Lists}
import org.scalatest.{FlatSpec, Matchers}

class PrismSpec extends FlatSpec with Matchers {
  import Prism._
  import Traversal._
  sealed trait NetworkResult
  case class NetworkSuccess(result: String) extends NetworkResult
  case object NetworkFailure extends NetworkResult

  val networkSuccessPrism = simplerPrism[NetworkResult, NetworkSuccess](identity, {
    case x:NetworkSuccess => Some(x)
    case _ => None
  })

  val successStringIso = Isomorphism.simpleIso[NetworkSuccess, String](_.result, NetworkSuccess(_))

  "Prism" should "allow mapping over a selection" in {
    val m: NetworkResult => NetworkResult =
      networkSuccessPrism.mapOf{case NetworkSuccess(result) => NetworkSuccess(result.toUpperCase)}
    m(NetworkSuccess("hello, world")) should be(NetworkSuccess("HELLO, WORLD"))
    m(NetworkFailure) should be(NetworkFailure)
  }

  it should "compose with Isomorphism" in {
    val i = Prism.compose(networkSuccessPrism, successStringIso)
    i.mapOf(_.toUpperCase)(NetworkSuccess("hello, world")) should be(NetworkSuccess("HELLO, WORLD"))
    i.mapOf(_.toUpperCase)(NetworkFailure) should be(NetworkFailure)
  }
  it should "compose with other Prism" in {
    val i = Prism.compose(Eithers.rightPrism[String, NetworkResult, NetworkResult], Prism.compose(networkSuccessPrism, successStringIso))
    i.mapOf(_.toUpperCase)(Right(NetworkSuccess("hello, world"))) should be (Right(NetworkSuccess("HELLO, WORLD")))
    i.mapOf(_.toUpperCase)(Right(NetworkFailure)) should be (Right(NetworkFailure))
    i.mapOf(_.toUpperCase)(Left("hello, world")) should be (Left("hello, world"))
  }
  it should "compose with Traversal" in {
    val listTraversal = Lists.traversal[Either[Int, Char], Either[Int, String]]
    val traversal = Traversal.compose(listTraversal, Eithers.rightPrism[Int, Char, String] )
    val input = List(Left(1), Right('m'), Left(2), Right('c'), Right('b'))
    traversal.mapOf(_ +: "at")(input) should be (List(
      Left(1), Right("mat"), Left(2), Right("cat"), Right("bat")
    ))
  }
  it should "be convertable to a getter (in the reverse direction) via re" in {
    val i = Prism.compose(Eithers.rightPrism[String, NetworkResult, NetworkResult], Prism.compose(networkSuccessPrism, successStringIso))
    val l = i.re
    l.get("hello, world") should be(Right(NetworkSuccess("hello, world")))
  }
  it should "be useable in a match statement" in {
    val rightPrism =  Eithers.rightPrism[Int, String, String]
    val right: Either[Int, String] = Right("hello, world")
    val left: Either[Int, String] = Left (1)
    (right match {
      case rightPrism(a) => a
      case _ => "default"
    }) should be ("hello, world")

    (left match {
      case rightPrism(a) => a
      case _ => "default"
    }) should be ("default")
  }
}
