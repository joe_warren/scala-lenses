package it.doscienceto.lens

import it.doscienceto.lens.concrete.{BinaryTree, Lists, Maps}
import org.scalatest.{FlatSpec, Matchers}

class IndexedTraversalSpec extends FlatSpec with Matchers {
  "An indexed Traversal" should "support modification with an index" in {
   val traversal =  Lists.traversal[String, String].enumerate
    val l = List("zero", "one", "two")
    traversal.indexedMapOf((i: Int, s: String) => s"$i:$s")(l) should be(
      List("0:zero", "1:one", "2:two")
    )
  }
  it should "compose, preserving both indices" in {
    val lol = List( List("A", "B"), List("C", "D"))
    val t1 = Lists.traversal[List[String], List[String]].enumerate
    val t2 = Lists.traversal[String, String].enumerate

    val traversal = IndexedTraversal.compose(t1, t2)

    val result: List[List[String]] =
      traversal.indexedMapOf{
        case ((i1: Int, i2: Int), s: String) => s"$i1,$i2:$s"
      }(lol)

    result should be( List(List("0,0:A", "0,1:B"), List("1,0:C","1,1:D")) )
  }
  it should "compose, preseving indices to the left or to the right" in {
    val lol = List( List("A", "B"), List("C", "D"))
    val t1 = Lists.traversal[List[String], List[String]]
    val t2 = Lists.traversal[String, String]

    // Note, this also works with two indexed traversals, due to subtyping
    val tLeft = IndexedTraversal.composeLeft(t1.enumerate, t2)
    val tRight = IndexedTraversal.composeRight(t1, t2.enumerate)

    def f( i: Int, s: String) = s"$i:$s"

    tLeft.indexedMapOf(f)(lol) should be( List(List("0:A", "0:B"), List("1:C","1:D")) )
    tRight.indexedMapOf(f)(lol) should be( List(List("0:A", "1:B"), List("0:C","1:D")) )
  }
  it should "support modifying maps using their keys" in {
    val traversal = Maps.values[String, Int, (String, Int)]
    val m = Map("one" -> 1, "two" -> 2, "three" -> 3)
    traversal.indexedMapOf{case (k, v) => (s"$k x two = $v x 2", v*2) }(m) should be (
      Map (
        "one" -> ("one x two = 1 x 2",2),
        "two" -> ("two x two = 2 x 2",4),
        "three" -> ("three x two = 3 x 2",6)
      )
    )
  }
  it should "lets just compose a map traversal with a list one, to flex" in {
    val t1 = Maps.values[String, List[Boolean], List[String]]
    val t2 = Lists.traversal[Boolean, String].enumerate
    val traversal = IndexedTraversal.compose(t1, t2)
    val mapOfLists = Map(
      "one" -> List(true, false, true),
      "two" -> List(true, true)
    )
    val result = traversal.indexedMapOf {
      case ((i1 , i2), v) => s"($i1, $i2) => $v"
    }(mapOfLists)
    result should be( Map(
        "one" -> List(
          "(one, 0) => true",
          "(one, 1) => false",
          "(one, 2) => true"
        ),
        "two" -> List(
          "(two, 0) => true",
          "(two, 1) => true",
        )
    ))
  }
  it should "support creating a new traversal from a predicate on the indices" in {
    val traversal = Lists.traversal[Int, Int].enumerate.elements(_ % 2 == 0)
    val l = List(3, 4, 5, 6, 7)
    traversal.mapOf(_*2)(l) should be (List(
      6, 4, 10, 6, 14
    ))
  }
  it should "enable traversing a binary tree at a specific depth" in{
    val traversal = BinaryTree.layer[String](2)
    val tree = BinaryTree.Branch(
      BinaryTree.Branch(
        BinaryTree.branchLeft(
          BinaryTree.singleton("at layer 3"),
          "at layer 2"
        ),
        "at Layer 1",
        BinaryTree.singleton("also at layer 2")
      ),
      "at layer 0",
      BinaryTree.branchLeft(
        BinaryTree.singleton("also, also at layer 2"),
        "at layer 1"
      )
    )
    traversal.buildListOf(tree) should be (List(
      "at layer 2", "also at layer 2", "also, also at layer 2"
    ))
  }
  it should "index a binary tree with a list of directions" in {
    val tree = BinaryTree.Branch(
      BinaryTree.branchLeft(
        BinaryTree.singleton(0),
        1
      ),
      2,
      BinaryTree.branchRight(
        3,
        BinaryTree.singleton(4)
      )
    )

    def printPath(path: BinaryTree.Index): String = path.map{
      case BinaryTree.L => "L"
      case BinaryTree.R => "R"
    }.mkString(",")

    val result = BinaryTree.traversal[Int, String].indexedMapOf{
      case (path, value) => s"${printPath(path)}:$value"
    }(tree)

    result should be( BinaryTree.Branch(
      BinaryTree.branchLeft(
        BinaryTree.singleton("L,L:0"),
        "L:1"
      ),
      ":2",
      BinaryTree.branchRight(
        "R:3",
        BinaryTree.singleton("R,R:4")
      )
    ))
  }
  it should "support mapping the index" in {
    val traversal =  Lists.traversal[String, String].enumerate.mapIndex(_*2)
    val l = List("zero", "one", "two")
    traversal.indexedMapOf((i: Int, s: String) => s"$i:$s")(l) should be(
      List("0:zero", "2:one", "4:two")
    )
  }

  it should "support mapping the index including the source" in {
    val traversal =  Lists.traversal[String, String].enumerate.mapIndexWithSource{ case (i, s) => s.length -i }
    val l = List("zero", "one", "two")
    traversal.indexedMapOf((i: Int, s: String) => s"$i:$s")(l) should be(
      List("3:zero", "2:one", "1:two")
    )
  }
}
