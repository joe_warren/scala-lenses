package it.doscienceto.lens
import concrete._
import org.scalatest.{FlatSpec, Matchers}

class OptionalSpec extends FlatSpec with Matchers {
  "headOptional" should "support getting the first value of a list" in {
    Lists.headOptional.getOption(List(1, 2, 3)) should be(Some(1))
  }
  it should "not get the first value of an empty List" in {
    Lists.headOptional.getOption(List.empty) should be(None)
  }
  it should "support modifying the first value of a list" in {
    Lists.headOptional[Int].mapOf(_ * 2)(List(1, 2, 3)) should be (List(2, 2, 3))
  }
  it should "not modify the first value of an empty list" in {
    Lists.headOptional[Int].mapOf(_ * 2)(List.empty) should be (List.empty)
  }
  "Optional" should "compose with lenses to make an optional" in {
    type M = Map[String, String]
    val lens = At.at[M, String, String]("key")
    val optional: SimpleOptional[M, String] = Optional.compose(lens, Option.optional)

    optional.mapOf(_.toUpperCase)(Map("key" -> "value", "k2" -> "v2")) should be{
      Map("key"->"VALUE", "k2"->"v2")
    }
  }
  it should "compose with traversals to make a traversal" in {
    val treeOfLists =
      NonEmptyTree.Branch(NonEmptyTree.Leaf(List(1, 2, 3)),
        NonEmptyTree.Branch(
          NonEmptyTree.Leaf(List(4, 5, 6)),
          NonEmptyTree.Branch(
            NonEmptyTree.Leaf(List.empty[Int]),
            NonEmptyTree.Leaf(List(7, 8))
          )
        )
      )
    val traversal: SimpleTraversal[NonEmptyTree.NonEmptyTree[List[Int]], Int] =
      Traversal.compose(NonEmptyTree.traversal[List[Int], List[Int]], Lists.headOptional[Int])
    traversal.buildListOf(treeOfLists) should be(List(1, 4, 7))
  }
  "The Indexed instance for lists" should "focus on an indexed item in a list" in {
    val listOfSix = (1 to 6).toList
    val listOfTwo = List(1, 2)
    val optional = Indexed.index[List[Int], Int, Int](3)
    optional.mapOf((_: Int) * 100)(listOfSix) should be (List(1, 2, 3, 400, 5, 6))
    optional.mapOf((_: Int) * 100)(listOfTwo) should be (List(1, 2))
  }
}
