package it.doscienceto.lens

import it.doscienceto.lens.typeclasses.instances._
import it.doscienceto.lens.typeclasses._
import it.doscienceto.lens.concrete.{NonEmptyTree, Tuples}
import org.scalatest.{FlatSpec, Matchers}

class NonEmptyTraversalSpec extends FlatSpec with Matchers {
  val tree = NonEmptyTree.Branch(NonEmptyTree.Leaf(1),
    NonEmptyTree.Branch(
      NonEmptyTree.Leaf(2),
      NonEmptyTree.Leaf(3)
    )
  )
  "Non Empty Traversals" should "aggregate semigroup values (via getters)" in {
    import Getter._
    val t1 = NonEmptyTree.traversal[Int, Sum].mapOf{Sum(_)}(tree)
    val t = NonEmptyTree.traversal[Sum, Sum].get(t1)
    t.value should be(6)
  }
  it should "get the head value" in {
    NonEmptyTree.traversal.headOf(tree) should be (1)
  }

  it should "foldOf over values" in {
    val t = NonEmptyTree.traversal.foldOf{ x: Int => y: Int => x+y}(0)(tree)
    t should be(6)
  }

  it should "let you coerce traversable objects to a List" in {
    val t = NonEmptyTree.traversal.buildListOf(tree)
    t should be(List(1, 2, 3))
  }

  it should "let you map over values" in {
    val t = NonEmptyTree.traversal.mapOf{ i:Int => (i*2).toString()}(tree)
    t should be(
      NonEmptyTree.Branch(NonEmptyTree.Leaf("2"),
        NonEmptyTree.Branch(
          NonEmptyTree.Leaf("4"),
          NonEmptyTree.Leaf("6")
        )
      )
    )
  }
  it should "support traversing both sides of a tuple" in {
    Tuples.both[Int, Int].mapOf(_*2)((1, 2)) should be ((2, 4))
  }

  it should ("support traversing directly") in {
    val r = NonEmptyTree.traversal[Int, Unit].apply(x => Writer(List(x), ())).apply(tree)
    r.written should be(List(1, 2, 3))
  }

  it should ("support traversing in reverse") in {
    val r = NonEmptyTree.traversal[Int, Int].reverse.buildListOf(tree)
    r should be(List(3, 2, 1))
  }

  it should ("compose with traversals") in {
    val nestedTree = NonEmptyTree.traversal[Int, NonEmptyTree.NonEmptyTree[Int]].mapOf(_ => tree)(tree)

    val composed = Traversal.compose(
      NonEmptyTree.traversal[NonEmptyTree.NonEmptyTree[Int], NonEmptyTree.NonEmptyTree[Int]],
      NonEmptyTree.traversal[Int, Int]
    )
    val list = composed.buildListOf(nestedTree)
    list should be (List(1,2,3,1,2,3,1,2,3))
  }

  it should "support creating a lens, over the head of the items, when monomorphic" in {
    val lens = NonEmptyTree.traversal[Int, Int].head
    lens.get(tree) should be(1)
    lens.set(tree, 10) should be(
      NonEmptyTree.Branch(NonEmptyTree.Leaf(10),
        NonEmptyTree.Branch(
          NonEmptyTree.Leaf(2),
          NonEmptyTree.Leaf(3)
        )
      )
    )
  }
}
