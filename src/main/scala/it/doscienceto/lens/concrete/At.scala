package it.doscienceto.lens.concrete

import it.doscienceto.lens.{Optional, SimpleLens, SimpleOptional}
import it.doscienceto.lens.typeclasses._
import it.doscienceto.lens.typeclasses.Syntax._
import it.doscienceto.lens.typeclasses.instances._

trait At[A, Ix, V] extends Indexed[A, Ix, V]{
  def at(i: Ix): SimpleLens[A, Option[V]]
  def index(i: Ix): SimpleOptional[A, V] = Optional.compose(at(i), Option.optional)
}

object At {
  implicit def atForMap[K, V]: At[Map[K, V], K, V] = (k: K) => new SimpleLens[Map[K, V], Option[V]] {
    override def applyLens[F[_] : Functor](f: Option[V] => F[Option[V]]): Map[K, V] => F[Map[K, V]] = { m =>
      implicitly[Functor[F]].fmap[Option[V], Map[K, V]]({
        case Some(v) => m.updated(k, v)
        case None => m.removed(k)
      })(f(m.get(k)))
    }
  }
  def at[A, Ix, V](i: Ix)(implicit inst: At[A, Ix, V]): SimpleLens[A, Option[V]] = inst.at(i)
}
