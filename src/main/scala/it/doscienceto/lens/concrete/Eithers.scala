package it.doscienceto.lens.concrete

import it.doscienceto.lens.{Lens, Prism}

object Eithers {
  import Prism._

  def leftPrism[LA, LB, R]: Prism[Either[LA, R], Either[LB, R], LA, LB] =
    prism[Either[LA, R], Either[LB, R], LA, LB](
      Left(_), {
        case Left(l) => Right(l)
        case Right(r) => Left(Right(r))
      })
  def rightPrism[L, RA, RB]: Prism[Either[L, RA], Either[L, RB], RA, RB] =
    prism[Either[L, RA], Either[L, RB], RA, RB](
      Right(_), {
        case Left(l) => Left(Left(l))
        case Right(r) => Right(r)
      })
  def bothLens[A, B]: Lens[Either[A, A], Either[B, B], A, B] =
    Lens.lens[Either[A, A], Either[B, B], A, B](_.merge, {
      case Left(_) => Left(_)
      case Right(_) => Right(_)
    })
}
