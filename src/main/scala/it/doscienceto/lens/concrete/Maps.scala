package it.doscienceto.lens.concrete

import it.doscienceto.lens.typeclasses.{Applicative, Indexable}
import it.doscienceto.lens.typeclasses.Syntax._
import it.doscienceto.lens.IndexedTraversal
object Maps {
  def values[K, A, B]: IndexedTraversal[K, Map[K, A], Map[K, B], A, B] =
    new IndexedTraversal[K, Map[K, A], Map[K, B], A, B] {
      override def applyIndexedTraversal[P[_, _]: Indexable[K, *[_,_]], F[_]: Applicative]
      (p: P[A, F[B]]): Map[K, A] => F[Map[K, B]] = { map: Map[K, A] =>
        val acc = Applicative[F].pure(Map.empty[K, B])
        map.foldRight(acc) { case ((k, a), mf) =>
          ( mf, p.indexed(k)(a) ).mapN( _.updated(k, _) )
        }
      }
    }
}
