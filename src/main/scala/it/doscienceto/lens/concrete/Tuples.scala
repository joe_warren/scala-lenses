package it.doscienceto.lens.concrete

import it.doscienceto.lens.typeclasses.Apply
import it.doscienceto.lens.typeclasses.Syntax._
import it.doscienceto.lens.{Lens, NonEmptyTraversal}

object Tuples {
  import Lens._
  def first[A1, A2, B]: Lens[(A1, B), (A2, B), A1, A2] = lens(
    {case (a, _) => a},
    {case (_, b) => {a2 => (a2, b)}}
  )
  def second[A, B1, B2]: Lens[(A, B1), (A, B2), B1, B2] = lens(
    {case (_, b) => b},
    {case (a, _) => b2 => (a, b2)}
  )
  /* I don't know why you need this type synonym here, but it helps */
  type Pair[A] = (A, A)
  def both[A, B]: NonEmptyTraversal[Pair[A], Pair[B], A, B] = new NonEmptyTraversal[Pair[A], Pair[B], A, B]{
    override def applyNonEmptyTraversal[F[_] : Apply](f: A => F[B]): Pair[A] => F[Pair[B]] = { case (x1, x2) =>
      (f(x1), f(x2)).mapN((_,_))
    }
  }
}
