package it.doscienceto.lens.concrete

import it.doscienceto.lens.typeclasses._
import it.doscienceto.lens.typeclasses.Syntax._
import it.doscienceto.lens.typeclasses.instances._
import it.doscienceto.lens.{IndexedTraversal, Traversal}

object BinaryTree {
  sealed trait BinaryTree[A]
  case class Branch[A](
                        left: BinaryTree[A],
                        value: A,
                        right: BinaryTree[A]
                      ) extends BinaryTree[A]
  case class Leaf[A]() extends BinaryTree[A]

  def singleton[A](value: A) = Branch[A](Leaf[A], value, Leaf[A])
  def branchLeft[A](left: BinaryTree[A], value: A) = Branch[A](left, value, Leaf[A])
  def branchRight[A](value: A, right: BinaryTree[A]) = Branch[A](Leaf[A], value, right)

  val L = Left(())
  val R = Right(())
  type Direction = Either[Unit, Unit]
  type Index = List[Direction]
  def traversal[A, B]: IndexedTraversal[Index, BinaryTree[A], BinaryTree[B], A, B] =
    new IndexedTraversal[Index, BinaryTree[A], BinaryTree[B], A, B]{
      override def applyIndexedTraversal[P[_, _]: Indexable[Index, *[_,_]], F[_] : Applicative](p: P[A, F[B]]): BinaryTree[A] => F[BinaryTree[B]] = {
        case tree =>
          def atNode(path: Index, node: BinaryTree[A]): F[BinaryTree[B]] = { node match {
            case Leaf() => implicitly[Applicative[F]].pure[BinaryTree[B]](Leaf[B])
            case Branch(left, value, right) =>
              implicitly[Applicative[F]].fmap3[BinaryTree[B], B, BinaryTree[B], BinaryTree[B]] {
                (l, v, r) => Branch[B](l, v, r)
              }(
                atNode(path.appended(L), left),
                implicitly[Indexable[Index, P]]
                  .indexed(p)(path)(value),
                atNode(path.appended(R), right)
              )
          }}
          atNode(List.empty, tree)
      }
    }
    def layer[A](n: Int): IndexedTraversal[Index, BinaryTree[A], BinaryTree[A], A, A] =
      traversal[A, A].elements(_.length == n)

}
