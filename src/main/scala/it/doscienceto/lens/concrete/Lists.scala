package it.doscienceto.lens.concrete

import it.doscienceto.lens.typeclasses.Applicative
import it.doscienceto.lens.typeclasses.Syntax._
import it.doscienceto.lens.{Optional, SimpleOptional, Traversal}

object Lists {
  def traversal[A, B]() : Traversal[List[A], List[B], A, B] =
    new Traversal[List[A], List[B], A, B] {
    override def applyTraversal[F[_] : Applicative](f: A => F[B]): List[A] => F[List[B]] = {
      case List() => Applicative[F].pure(List.empty[B])
      case (x+:xs) => (f(x), apply[F](f).apply(xs)).mapN(_+:_)
    }
  }
  def headOptional[A]: SimpleOptional[List[A], A]=
    Optional.simplerOptional[List[A], A](
      _.headOption,
      {(v:A, l:List[A]) => l match {
        case _::t => v :: t
        case Nil => List.empty
      }}
    )
}
