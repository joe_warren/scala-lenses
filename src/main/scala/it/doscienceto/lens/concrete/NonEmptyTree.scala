package it.doscienceto.lens.concrete

import it.doscienceto.lens.typeclasses._
import it.doscienceto.lens.typeclasses.Syntax._
import it.doscienceto.lens.{ IndexedNonEmptyTraversal, IndexedTraversal}

object NonEmptyTree {
  sealed trait NonEmptyTree[A]
  case class Branch[A](left: NonEmptyTree[A], right: NonEmptyTree[A]) extends NonEmptyTree[A]
  case class Leaf[A](value: A) extends NonEmptyTree[A]

  val L = Left(())
  val R = Right(())
  type Direction = Either[Unit, Unit]
  type Index = List[Direction]
  def traversal[A, B]: IndexedNonEmptyTraversal[Index, NonEmptyTree[A], NonEmptyTree[B], A, B] =
    new IndexedNonEmptyTraversal[Index, NonEmptyTree[A], NonEmptyTree[B], A, B] {
      override def applyIndexedNonEmptyTraversal[P[_, _]: Indexable[Index, *[_,_]], F[_] : Apply](p: P[A, F[B]]): NonEmptyTree[A] => F[NonEmptyTree[B]] = {
        tree =>
          def atNode(path: Index, node: NonEmptyTree[A]): F[NonEmptyTree[B]] = {
            node match {
              case Leaf(value) => p.indexed(path)(value).map(Leaf[B])
              case Branch(left, right) =>
                (
                  atNode(path.appended(L), left),
                  atNode(path.appended(R), right)
                ).mapN(Branch[B](_, _))
            }
          }

          atNode(List.empty, tree)
      }
    }

  def layer[A](n: Int): IndexedTraversal[Index, NonEmptyTree[A], NonEmptyTree[A], A, A] =
    traversal[A, A].elements(_.length == n)
}
