package it.doscienceto.lens.concrete
import it.doscienceto.lens.Optional

object Option {
  def optional[A, B]: Optional[Option[A], Option[B], A, B] = Optional.optional(
    _.toRight(None),
    {(b: B, o: Option[A]) => o.map(_ => b)}
  )
}
