package it.doscienceto.lens.concrete

import it.doscienceto.lens.typeclasses.Applicative
import it.doscienceto.lens.{Lens, SimpleTraversal}

object Bitwise {
  def bitAt(index: Int) = Lens.simpleLens[Int, Boolean](
    {case i: Int => (i & (1<<index)) != 0},
    {i => {
      case true => i | (1<<index)
      case false => i & ~(1<<index)
    }}
  )
  def bits = new SimpleTraversal[Int, Boolean] {
    override def applyTraversal[F[_] : Applicative](f: Boolean => F[Boolean]): Int => F[Int] = {
      case i =>
        val bitParts = (0 to 31).map { index =>
           Applicative[F].fmap[Boolean, Int] {
             case true => 1 << index
             case false => 0
           }(f((i & (1<<index)) != 0))
        }
        bitParts.reduce(Applicative[F].fmap2[Int, Int, Int]{ case (a, b) => a | b})
    }
  }
}
