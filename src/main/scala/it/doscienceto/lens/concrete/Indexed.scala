package it.doscienceto.lens.concrete

import it.doscienceto.lens.typeclasses.Pointed
import it.doscienceto.lens.typeclasses.Syntax._
import it.doscienceto.lens.SimpleOptional

trait Indexed[A, Ix, V] {
  def index(i: Ix): SimpleOptional[A, V]
}

object Indexed {
  def index[A, Ix, V](i: Ix)(implicit inst: Indexed[A, Ix, V]): SimpleOptional[A, V] = inst.index(i)

  implicit def indexedForList[V]: Indexed[List[V], Int, V] = (i: Int) => new SimpleOptional[List[V], V] {
    override def applyOptional[F[_] : Pointed](f: V => F[V]): List[V] => F[List[V]] = { list =>
      i match {
        case list(value) => f(value).map(list.updated(i, _))
        case _ => Pointed[F].pure(list)
      }
    }
  }
}
