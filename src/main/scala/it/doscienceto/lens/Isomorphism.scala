package it.doscienceto.lens
import it.doscienceto.lens.typeclasses._
import it.doscienceto.lens.typeclasses.Syntax._
import it.doscienceto.lens.typeclasses.instances._


trait Isomorphism[S, T, A, B] extends Lens[S, T, A, B] with Prism[S, T, A, B] { that =>
  def applyIso[P[_, _]: Profunctor, F[_]: Functor](s: P[A, F[B]]): P[S, F[T]]
  def apply[P[_, _]: Profunctor, F[_]: Functor](s: P[A, F[B]]): P[S, F[T]] = applyIso(s)

  override def applyLens[F[_] : Functor](f: A => F[B]): (S => F[T]) = applyIso(f)
  override def applyPrism[C[_, _]: Choice, F[_]: Pointed](s: C[A, F[B]]): C[S, F[T]] = applyIso(s)

  def withIso[R](f:(S=>A, B=>T)=> R): R = {
    this.apply[Exchange[*, *, A, B], Identity](Exchange[A, Identity[B],A, B](identity, Identity[B])) match {
      case e:Exchange[S, Identity[T], A, B] => f.apply(e.sa, e.bt.andThen({case Identity(b)=> b}))
    }
  }

  def inverse: Isomorphism[B, A, T, S] = {
    this.withIso[Isomorphism[B, A, T, S]]{case (sa, bt) => Isomorphism.iso(bt, sa)}
  }

  def view(s: S): A = withIso[A]{case (sa, _) => sa.apply(s)}
  def over(f: A=>B)(s: S): T = withIso[T]{case (sa, bt) => sa.andThen(f).andThen(bt).apply(s)}
}

object Isomorphism {
  def iso[S, T, A, B](fwd: S=>A, back: B => T): Isomorphism[S, T, A, B] = new Isomorphism[S, T, A, B]{
    override def applyIso[P[_, _] : Profunctor, F[_] : Functor](s: P[A, F[B]]): P[S, F[T]] = {
      implicitly[Profunctor[P]].dimap[S, A, F[B], F[T]](fwd, implicitly[Functor[F]].fmap[B, T](back)(_))(s)
    }
  }
  def simpleIso[S, A](fwd: S => A, back: A => S): SimpleIsomorphism[S, A] = iso(fwd, back)

  def compose[A, B, S1, T1, S2, T2](i1: Isomorphism[S2, T2, S1, T1], i2: Isomorphism[S1, T1, A, B]): Isomorphism[S2, T2, A, B] = new Isomorphism[S2, T2, A, B] {
    override def applyIso[P[_, _] : Profunctor, F[_] : Functor](s: P[A, F[B]]): P[S2, F[T2]] = i1(i2(s))
  }
}
