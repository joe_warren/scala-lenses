package it.doscienceto.lens

import it.doscienceto.lens.typeclasses._
import it.doscienceto.lens.typeclasses.Syntax._
import it.doscienceto.lens.typeclasses.instances._

/* This is sometimes called Affine
 */
trait Optional[S, T, A, B] extends Traversal[S, T, A, B] {
  def applyOptional[F[_] : Pointed](f: A => F[B]): (S => F[T])

  def apply[F[_] : Pointed](f: A => F[B]): (S => F[T]) = applyOptional(f)
  override def applyTraversal[F[_] : Applicative](f: A => F[B]): S => F[T] = applyOptional(f)

  def getOption(fa: S): Option[A] = this.apply { case a => Left(a).withRight[B] }(Monad[Either[A, *]]).apply(fa).left.toOption

  // this lets us pattern match on a prism/optional
  def unapply(s: S): Option[A] = getOption(s)
}

object Optional {
  def optional[S, T, A, B](seta: S => Either[T, A], bst: (B, S) => T): Optional[S, T, A, B] = new Optional[S, T, A, B] {
    override def applyOptional[F[_] : Pointed](f: A => F[B]): S => F[T] = { s =>
      seta(s) match {
        case Left(t) => implicitly[Pointed[F]].pure(t)
        case Right(a) => implicitly[Pointed[F]].fmap({b:B => bst(b, s)})(f(a))
      }
    }
  }
  def simpleOptional[S, A](getOrModify: S => Either[S, A], set: (A, S)=>S): Optional[S, S, A, A] =
    optional[S, S, A, A](getOrModify, set)
  def simplerOptional[S, A](get: S => Option[A], set: (A, S)=>S): Optional[S, S, A, A] =
    simpleOptional({s: S => get(s).toRight(s)}, set)
  def compose[A, B, S1, T1, S2, T2](l1: Optional[S2, T2, S1, T1], l2: Optional[S1, T1, A, B]): Optional[S2, T2, A, B] = new Optional[S2, T2, A, B]{
    override def applyOptional[F[_] : Pointed](f: A => F[B]): S2 => F[T2] = l1(l2(f))
  }
}
