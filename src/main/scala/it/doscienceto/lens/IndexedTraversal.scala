package it.doscienceto.lens

import it.doscienceto.lens.typeclasses._
import it.doscienceto.lens.typeclasses.Syntax._
import it.doscienceto.lens.typeclasses.instances._

trait IndexedTraversal[I, S, T, A, B] extends Traversal [S, T, A, B] { that =>
  def apply[P[_, _]: Indexable[I, *[_,_]], F[_]: Applicative]
      (p: P[A, F[B]]): S => F[T] = applyIndexedTraversal(p)
  def applyIndexedTraversal[P[_, _]: Indexable[I, *[_,_]], F[_]: Applicative]
      (p: P[A, F[B]]): S => F[T]

  override def applyTraversal[F[_] : Applicative](f: A => F[B]): S => F[T] = { s =>
    applyIndexedTraversal(f).apply(s)
  }

  def indexedApplicativeMapOf[F[_]: Applicative](f: (I, A) => F[B])(s: S): F[T] = applyIndexedTraversal[IndexedFunction[I, *, *], F] (
    IndexedFunction{ (i: I, a: A) => f(i, a) } ).apply(s)

  def indexedMapOf(f: (I, A) => B)(s: S): T = indexedApplicativeMapOf[Identity]({
    (i: I, a: A) => Identity(f(i, a))
  })(s).value


  def mapIndexWithSource[I2](f: (I, S) => I2): IndexedTraversal[I2, S, T, A, B] =  new IndexedTraversal[I2, S, T, A, B] {
    import MappedIndexable._
    override def applyIndexedTraversal[P[_, _]: Indexable[I2, *[_,_]], F[_]: Applicative]
    (p: P[A, F[B]]): S => F[T] = { s: S =>
      that.applyIndexedTraversal[MappedIndexable[I2, I, *, *, P], F](
        MappedIndexable[I2, I, A, F[B], P](f(_, s), p)).apply(s)
    }
  }

  def mapIndex[I2](f: I => I2): IndexedTraversal[I2, S, T, A, B] =  mapIndexWithSource{ case (i, _) => f(i) }

}

object IndexedTraversal {

  implicit class MonomorphicIndexedTraversal[I, S, T, A](traversal: IndexedTraversal[I, S, T, A, A]){
    def elements(f: I => Boolean): IndexedTraversal[I, S, T, A, A] =
      new IndexedTraversal[I, S, T, A, A] {
        override def applyIndexedTraversal[P[_, _]: Indexable[I, *[_,_]], F[_]: Applicative]
        (p: P[A, F[A]]): S => F[T] =
          traversal.applyIndexedTraversal[IndexedFunction[I, *, *], F](
            IndexedFunction[I, A, F[A]]{ case (i, a) =>
              if(f(i)){
                p.indexed(i)(a)
              } else {
                Applicative[F].pure(a)
              }
            }
          )
      }
  }

  def composeIndices[I, I1, I2, A, B, S1, T1, S2, T2](fi: (I1, I2)=> I)(l1: IndexedTraversal[I1, S2, T2, S1, T1], l2: IndexedTraversal[I2, S1, T1, A, B]):
  IndexedTraversal[I, S2, T2, A, B] =
    new IndexedTraversal[I, S2, T2, A, B] {
      override def applyIndexedTraversal[P[_, _] : Indexable[I, *[_,_]], F[_] : Applicative](p: P[A, F[B]]): S2 => F[T2] =
        l1.applyIndexedTraversal[IndexedFunction[I1, *, *], F](
          IndexedFunction[I1, S1, F[T1]]{ (i1: I1, s1: S1) =>
            l2.applyIndexedTraversal[IndexedFunction[I2, *, *], F](
              IndexedFunction[I2, A, F[B]] { (i2: I2, a: A) =>
                implicitly[Indexable[I, P]].indexed(p)(fi(i1, i2))(a)
              }
            ).apply(s1)
          }
        )
    }

  def compose[I1, I2, A, B, S1, T1, S2, T2](l1: IndexedTraversal[I1, S2, T2, S1, T1], l2: IndexedTraversal[I2, S1, T1, A, B]):
    IndexedTraversal[(I1, I2), S2, T2, A, B] = composeIndices[(I1, I2), I1, I2, A, B, S1, T1, S2, T2](
      (i1: I1, i2: I2) => (i1, i2)
    )(l1, l2)

  def composeLeft[I, A, B, S1, T1, S2, T2](l1: IndexedTraversal[I, S2, T2, S1, T1], l2: Traversal[S1, T1, A, B]):
  IndexedTraversal[I, S2, T2, A, B] =
    new IndexedTraversal[I, S2, T2, A, B] {
      override def applyIndexedTraversal[P[_, _]: Indexable[I, *[_,_]], F[_] : Applicative](p: P[A, F[B]]): S2 => F[T2] =
        l1.applyIndexedTraversal[IndexedFunction[I, *, *], F](
          IndexedFunction[I, S1, F[T1]] { (i: I, s1: S1) =>
            l2.applyTraversal[F] { a: A =>
              implicitly[Indexable[I, P]].indexed(p)(i)(a)
            }.apply(s1)
          })
    }

  def composeRight[I, A, B, S1, T1, S2, T2](l1: Traversal[S2, T2, S1, T1], l2: IndexedTraversal[I, S1, T1, A, B]):
  IndexedTraversal[I, S2, T2, A, B] =
    new IndexedTraversal[I, S2, T2, A, B] {
      override def applyIndexedTraversal[P[_, _]: Indexable[I, *[_,_]], F[_]: Applicative](p: P[A, F[B]]): S2 => F[T2] =
        l1.applyTraversal[F]{ s1: S1 =>
          l2.applyIndexedTraversal[IndexedFunction[I, *, *], F](
            IndexedFunction[I, A, F[B]] { (i: I, a: A) =>
              implicitly[Indexable[I, P]].indexed(p)(i)(a)
            }
          ).apply(s1)
        }
    }
}
