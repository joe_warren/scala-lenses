package it.doscienceto.lens

import it.doscienceto.lens.typeclasses._
import it.doscienceto.lens.typeclasses.Syntax._
import it.doscienceto.lens.typeclasses.instances._

trait Lens[S, T,  A, B] extends NonEmptyTraversal [S, T, A, B] with Optional[S, T, A, B] {
  def applyLens[F[_] : Functor](f: A => F[B]): (S => F[T])
  def apply[F[_] : Functor](f: A => F[B]): (S => F[T]) = applyLens(f)

  override def applyNonEmptyTraversal[F[_] : Apply](f: A => F[B]): (S => F[T]) = applyLens(f)
  override def applyOptional[F[_] : Pointed](f: A => F[B]): (S => F[T]) = applyLens(f)

  def get(s: S): A = apply(Const[A, B](_)).apply(s).value
  def modify(f: A => B): (S => T) = s => apply(a => Identity(f(a))).apply(s).value
  def set(s: S, b: B): T = modify(_ => b)(s)
}

object Lens {
  def compose[S1, T1 , S2, T2, A, B](l1: Lens[S1, T1, S2, T2], l2: Lens[S2, T2, A, B]): Lens[S1, T1, A, B] = new Lens[S1, T1, A, B]{
    override def applyLens[F[_] : Functor](f: A => F[B]): S1 => F[T1] = l1(l2(f))
  }

  def lens[S, T, A, B](getter: S => A, setter: S => B => T): Lens[S, T , A, B] = new Lens[S, T, A, B] {
    override def applyLens[F[_] : Functor](f: A => F[B]): S => F[T] = s => f(getter(s)).map(setter(s))
  }

  def simpleLens[S, A](getter: S => A, setter: S => A => S): SimpleLens[S, A] = lens(getter, setter)
}

