package it.doscienceto.lens

import it.doscienceto.lens.typeclasses._
import it.doscienceto.lens.typeclasses.Syntax._
import it.doscienceto.lens.typeclasses.instances._

trait IndexedNonEmptyTraversal[I, S, T, A, B]
  extends IndexedTraversal[I, S, T, A, B]
    with NonEmptyTraversal[S, T, A, B] { that =>

  def apply[P[_, _]: Indexable[I, *[_,_]], F[_]: Apply]
  (p: P[A, F[B]]): S => F[T] = applyIndexedNonEmptyTraversal(p)

  def applyIndexedNonEmptyTraversal[P[_, _]: Indexable[I, *[_,_]], F[_]: Apply]
  (p: P[A, F[B]]): S => F[T]

  override def applyIndexedTraversal[P[_, _]: Indexable[I, *[_,_]], F[_]: Applicative]
  (p: P[A, F[B]]): S => F[T] = applyIndexedNonEmptyTraversal(p)

  override def applyNonEmptyTraversal[F[_] : Apply](f: A => F[B]): S => F[T] = { s =>
    applyIndexedNonEmptyTraversal(f).apply(s)
  }
  override def mapIndexWithSource[I2](f: (I, S) => I2): IndexedNonEmptyTraversal[I2, S, T, A, B] =
    new IndexedNonEmptyTraversal[I2, S, T, A, B] {
    import MappedIndexable._
    override def applyIndexedNonEmptyTraversal[P[_, _] : Indexable[I2, *[_,_]], F[_] : Apply]
    (p: P[A, F[B]]): S => F[T] = { s: S =>
      that.applyIndexedNonEmptyTraversal[MappedIndexable[I2, I, *, *, P], F](
        MappedIndexable[I2, I, A, F[B], P](f(_, s), p)).apply(s)
    }
  }
  override def mapIndex[I2](f: I => I2): IndexedNonEmptyTraversal[I2, S, T, A, B] =  mapIndexWithSource{ case (i, _) => f(i) }
}

object IndexedNonEmptyTraversal {
  def composeIndices[I, I1, I2, A, B, S1, T1, S2, T2](fi: (I1, I2)=> I)(l1: IndexedNonEmptyTraversal[I1, S2, T2, S1, T1], l2: IndexedNonEmptyTraversal[I2, S1, T1, A, B]):
  IndexedNonEmptyTraversal[I, S2, T2, A, B] =
    new IndexedNonEmptyTraversal[I, S2, T2, A, B] {
      override def applyIndexedNonEmptyTraversal[P[_, _]: Indexable[I, *[_,_]], F[_] : Apply](p: P[A, F[B]]): S2 => F[T2] =
        l1.applyIndexedNonEmptyTraversal[IndexedFunction[I1, *, *], F](
          IndexedFunction[I1, S1, F[T1]]{ (i1: I1, s1: S1) =>
            l2.applyIndexedNonEmptyTraversal[IndexedFunction[I2, *, *], F](
              IndexedFunction[I2, A, F[B]] { (i2: I2, a: A) =>
                p.indexed(fi(i1, i2))(a)
              }
            ).apply(s1)
          }
        )
    }

  def compose[I1, I2, A, B, S1, T1, S2, T2](l1: IndexedNonEmptyTraversal[I1, S2, T2, S1, T1], l2: IndexedNonEmptyTraversal[I2, S1, T1, A, B]):
  IndexedNonEmptyTraversal[(I1, I2), S2, T2, A, B] = composeIndices[(I1, I2), I1, I2, A, B, S1, T1, S2, T2](
    (i1: I1, i2: I2) => (i1, i2)
  )(l1, l2)

  def composeLeft[I, A, B, S1, T1, S2, T2](l1: IndexedNonEmptyTraversal[I, S2, T2, S1, T1], l2: NonEmptyTraversal[S1, T1, A, B]):
  IndexedNonEmptyTraversal[I, S2, T2, A, B] =
    new IndexedNonEmptyTraversal[I, S2, T2, A, B] {
      override def applyIndexedNonEmptyTraversal[P[_, _]: Indexable[I, *[_,_]], F[_] : Apply](p: P[A, F[B]]): S2 => F[T2] =
        l1.applyIndexedNonEmptyTraversal[IndexedFunction[I, *, *], F](
          IndexedFunction[I, S1, F[T1]] { (i: I, s1: S1) =>
            l2.applyNonEmptyTraversal[F] { a: A =>
              p.indexed(i)(a)
            }.apply(s1)
          })
    }
  def composeRight[I, A, B, S1, T1, S2, T2](l1: NonEmptyTraversal[S2, T2, S1, T1], l2: IndexedNonEmptyTraversal[I, S1, T1, A, B]):
  IndexedNonEmptyTraversal[I, S2, T2, A, B] =
    new IndexedNonEmptyTraversal[I, S2, T2, A, B] {
      override def applyIndexedNonEmptyTraversal[P[_, _]: Indexable[I, *[_,_]], F[_] : Apply](p: P[A, F[B]]): S2 => F[T2] =
        l1.applyNonEmptyTraversal[F]{ s1: S1 =>
          l2.applyIndexedNonEmptyTraversal[IndexedFunction[I, *, *], F](
            IndexedFunction[I, A, F[B]] { (i: I, a: A) =>
              p.indexed(i)(a)
            }
          ).apply(s1)
        }
    }
}
