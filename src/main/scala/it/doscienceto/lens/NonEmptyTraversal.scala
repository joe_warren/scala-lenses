package it.doscienceto.lens

import it.doscienceto.lens.typeclasses._
import it.doscienceto.lens.typeclasses.Syntax._
import it.doscienceto.lens.typeclasses.instances._

/* Non Empty Traversal, lens calls this Traversal1 */
trait NonEmptyTraversal[S, T, A, B] extends Traversal[S, T, A, B] { that =>
  def applyNonEmptyTraversal[F[_] : Apply](f: A => F[B]): (S => F[T])

  def apply[F[_] : Apply](f: A => F[B]): (S => F[T]) = applyNonEmptyTraversal(f)
  override def applyTraversal[F[_] : Applicative](f: A => F[B]): S => F[T] = this.applyNonEmptyTraversal(f)

  def foldMapOf[M: Semigroup](f: A => M)(s: S): M = applyNonEmptyTraversal{ a => Const[M, B](f(a))}.apply(s).value

  // Because unapplied is only lawful when the function is associative
  // This strictly speaking requires the function to be associative
  def reduceOf(f: A => A => A)(s: S) = foldMapOf[Unapplied[A]](Unapplied(_))(s).apply(f)

  def headOf(s: S): A = foldMapOf[First[A]](First(_))(s).value

  override def reverse = new NonEmptyTraversal[S, T, A, B]{
    override def applyNonEmptyTraversal[F[_] : Apply](f: A => F[B]): S => F[T] = {
      s: S => that.applyNonEmptyTraversal{ a:A => Backwards[F, B](f(a))}.apply(s).underlyingFunctor
    }
  }

  override def enumerate: IndexedNonEmptyTraversal[Int, S, T, A, B] = new IndexedNonEmptyTraversal[Int, S, T, A, B] {
    override def applyIndexedNonEmptyTraversal[P[_, _]: Indexable[Int, *[_,_]], F[_]: Apply]
    (p: P[A, F[B]]): S => F[T] = { s: S =>
      that.applyNonEmptyTraversal[Enumerating[F, *]]{ a =>
        Enumerating[F, B]{ i =>
          (i+1,
            implicitly[Indexable[Int, P]].indexed(p)(i)(a))
        }
      }.apply(s).run(0)._2
    }
  }
}

object NonEmptyTraversal {
  implicit class MonomorphicNonEmptyTraversal[S, T, A](traversal: NonEmptyTraversal[S, T, A, A]){
    // This is similar to "singular" from lens, but less efficiently written
    def head: Lens[S, T, A, A] = Lens.lens(
      {s: S => traversal.headOf(s)},
      { s: S => a: A =>
        traversal.enumerate.elements(_==0).mapOf(_ => a)(s) }
    )
  }


  def compose[A, B, S1, T1, S2, T2](l1: NonEmptyTraversal[S2, T2, S1, T1], l2: NonEmptyTraversal[S1, T1, A, B]) = new NonEmptyTraversal[S2, T2, A, B]{
    override def applyNonEmptyTraversal[F[_] : Apply](f: A => F[B]): S2 => F[T2] = l1(l2(f))
  }
}
