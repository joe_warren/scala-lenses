package it.doscienceto.lens.typeclasses

trait Monoid[A] extends Semigroup[A] {
  def zero: A
}

trait MonoidSyntax{
  implicit class MonoidListOps[A: Monoid](list: List[A]){
    def mconcat: A = list.fold(Monoid[A].zero)(Monoid[A].accumulate(_, _))
  }
}

object Monoid {
  def apply[A](implicit monoid: Monoid[A]): Monoid[A] = monoid


  implicit def listMonoid[A]: Monoid[List[A]] = new Monoid[List[A]] {
    override def zero: List[A] = List.empty[A]
    override def accumulate(a: List[A], b: List[A]): List[A] = a ++ b
  }

  implicit def optionMonoid[A]: Monoid[Option[A]] = new Monoid[Option[A]]{
    override def zero: Option[A] = None
    override def accumulate(a: Option[A], b: Option[A]): Option[A] = a orElse b
  }
}