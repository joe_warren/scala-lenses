package it.doscienceto.lens.typeclasses

trait Profunctor[F[_, _]] {
  def dimap [A, B, C, D](f1: A => B, f2 :C => D)(fa: F[B, C]): F[A,D]
  def rmap[A, B, C](f: B => C) : F[A, B] => F[A, C] = dimap[A, A, B, C](identity, f)
  def lmap[A, B, C](f: A => B): F[B, C] => F[A, C] = dimap[A, B, C, C](f, identity)
}

trait ProfunctorSyntax{
  implicit class ProfunctorOps[F[_, _]: Profunctor, B, C](instance: F[B, C]){
    def dimap[A, D](f1: A => B, f2 :C => D): F[A, D] = Profunctor[F].dimap(f1, f2)(instance)
    def rmap[D](f: C => D): F[B, D] = Profunctor[F].rmap(f)(instance)
    def lmap[A](f: A => B): F[A, C] = Profunctor[F].lmap(f)(instance)
  }
}

object Profunctor {
  def apply[F[_, _]](implicit profunctor: Profunctor[F]): Profunctor[F] = profunctor

  implicit def functionIsProfunctor[I]: Profunctor[Function1] =
    new Profunctor[Function1] {
      override def dimap[A, B, C, D](f1: A => B, f2: C => D)(fa: B => C): A => D = f1 andThen fa andThen f2
    }
  implicit def profunctorIsFunctor[I, F[_, _]](implicit p: Profunctor[F]): Functor[F[I, *]] = new Functor[F[I, *]] {
    override def fmap[A, B](f: A => B)(a: F[I, A]): F[I, B] = p.rmap[I, A, B](f)(a)
  }
  implicit def profunctorIsContravariant[I, F[_, _]](implicit p: Profunctor[F]): Contravariant[F[*, I]] = new Contravariant[F[*, I]] {
    override def contramap[A, B](f: A => B)(b: F[B, I]): F[A, I] = p.lmap[A, B, I](f)(b)
  }
}
