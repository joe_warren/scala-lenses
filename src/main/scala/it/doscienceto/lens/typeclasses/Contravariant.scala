package it.doscienceto.lens.typeclasses

trait Contravariant[F[_]]{
  def contramap[A, B](f: A => B)(b: F[B]): F[A]
  def colift[A, B](f: A => B): F[B] => F[A] = contramap(f) _
}

trait ContravariantSyntax {
  implicit class ContravariantOps[F[_]: Contravariant, A](instance: F[A]){
    def contramap[B](f: B => A): F[B] = Contravariant[F].contramap(f)(instance)
  }
}

object Contravariant {
  def apply[F[_]](implicit contravariant: Contravariant[F]): Contravariant[F] = contravariant
}