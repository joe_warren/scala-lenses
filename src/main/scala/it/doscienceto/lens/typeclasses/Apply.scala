package it.doscienceto.lens.typeclasses

/* Adding an Apply Functor between Functor and Applicative
 * lets us write a form of Traversal that _must_ have one or more foci
 * because I don't think this exists elsewhere, let's call it an
 * Traversal1
 */
trait Apply[F[_]] extends Functor[F] {
  def ap[A, B](ff: F[A => B])(fa: F[A]): F[B]
  def fmap2[A, B, Z](f: (A, B) => Z)(fa: F[A], fb: F[B]): F[Z] = {
    val partiallyApplied = fmap[A, B => Z]{a: A => {
      b: B => f(a, b)
    }}(fa)
    ap[B, Z](partiallyApplied)(fb)
  }
  def fmap3[A, B, C, Z](f: (A, B, C) => Z)(fa: F[A], fb: F[B], fc: F[C]): F[Z] = {
    val c2z = fmap2((a: A, b: B) => f(a, b, _))(fa, fb)
    ap(c2z)(fc)
  }
}

trait ApplySyntax {
  implicit class ApplyFOps[F[_]: Apply, A, B](ff: F[A => B]){
    def ap(fa: F[A]): F[B] = Apply[F].ap(ff)(fa)
  }
  implicit class ApplyTuple2Ops[F[_]: Apply, A, B](tuple2: (F[A], F[B])){
    def mapN[Z](f: (A, B) => Z): F[Z] = Apply[F].fmap2(f)(tuple2._1, tuple2._2)
  }
  implicit class ApplyTuple3Ops[F[_]: Apply, A, B, C](tuple3: (F[A], F[B], F[C])){
    def mapN[Z](f: (A, B, C) => Z): F[Z] = Apply[F].fmap3(f)(tuple3._1, tuple3._2, tuple3._3)
  }
}

object Apply {

  def apply[F[_]](implicit applyTC: Apply[F]): Apply[F] = applyTC
}
