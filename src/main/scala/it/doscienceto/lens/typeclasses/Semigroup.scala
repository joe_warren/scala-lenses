package it.doscienceto.lens.typeclasses

trait Semigroup[A] {
  def accumulate(a: A, b: A): A
}

trait SemigroupSyntax{
  implicit class SemigroupOps[A: Semigroup](a: A){
    def <>(b: A): A = Semigroup[A].accumulate(a, b)
  }
}

object Semigroup {
  def apply[A](implicit semigroup: Semigroup[A]): Semigroup[A] = semigroup
}
