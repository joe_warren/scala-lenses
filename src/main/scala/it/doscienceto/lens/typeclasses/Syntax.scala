package it.doscienceto.lens.typeclasses

trait Syntax
  extends ApplySyntax
    with ChoiceSyntax
    with ContravariantSyntax
    with FunctorSyntax
    with ProfunctorSyntax
    with MonadSyntax
    with SemigroupSyntax
    with MonoidSyntax
    with IndexableSyntax {

}

object Syntax extends Syntax
