package it.doscienceto.lens.typeclasses


trait Choice[Ch[_, _]] extends Profunctor[Ch] {
  def left[A, B, C](p: Ch[A, B]): Ch[Either[A, C], Either[B, C]]
  def right[A, B, C](p: Ch[A, B]): Ch[Either[C, A], Either[C, B]]
}

trait ChoiceSyntax{
  implicit class ChoiceOps[Ch[_, _]: Choice, A, B](instance: Ch[A, B]){
    def left[C]: Ch[Either[A, C], Either[B, C]] = Choice[Ch].left[A, B, C](instance)
    def right[C]: Ch[Either[C, A], Either[C, B]] = Choice[Ch].right[A, B, C](instance)
  }
}

object Choice {
  def apply[Ch[_, _]](implicit choice: Choice[Ch]): Choice[Ch] = choice

  implicit def functionIsChoice: Choice[Function1] = new Choice[Function1] {
    override def dimap[A, B, C, D](f1: A => B, f2: C => D)(fa: B => C): A => D = f1 andThen fa andThen f2

    override def left[A, B, C](p: A => B): Either[A, C] => Either[B, C] = { e =>
      e.left.map(p)
    }

    override def right[A, B, C](p: A => B): Either[C, A] => Either[C, B] = { e =>
      e.map(p)
    }
  }
}
