package it.doscienceto.lens.typeclasses

/* Adding a Pointed Functor between Functor and Applicative
 * lets us to define an Optional Optic
 */
trait Pointed[F[_]] extends Functor[F] {
  def pure[A](a: A): F[A]
}

object Pointed {
  def apply[F[_]](implicit pointed: Pointed[F]): Pointed[F] = pointed
  def unit[F[_]](implicit pointed: Pointed[F]): F[_] = pointed.pure(())
}
