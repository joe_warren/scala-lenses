package it.doscienceto.lens.typeclasses

trait Functor[F[_]]{
  def fmap[A, B](f: A => B)(a: F[A]): F[B]

  def lift[A, B](f: A => B): F[A] => F[B] = fmap(f) _
}

trait FunctorSyntax{
  implicit class FunctorOps[F[_]: Functor, A](instance: F[A]){
    def map[B](f: A => B): F[B] = Functor[F].fmap(f)(instance)
  }
}

object Functor{
  def apply[F[_]](implicit functor: Functor[F]): Functor[F] = functor

  implicit def optionFunctor: Functor[Option] = new Functor[Option] {
    override def fmap[A, B](f: A => B)(fa: Option[A]): Option[B] = fa match {
      case Some(a) => Some(f(a))
      case None => None
    }
  }
}
