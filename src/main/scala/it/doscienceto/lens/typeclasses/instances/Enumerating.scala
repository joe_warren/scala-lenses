package it.doscienceto.lens.typeclasses.instances

import it.doscienceto.lens.typeclasses.{Applicative, Apply, Functor, Pointed}
import it.doscienceto.lens.typeclasses.Syntax._
// Enumerating is a State Transformer fixed at Int
// This is called Indexing in Lens
// But I found that name made it impossible to remember the difference between it, and Indexable
case class Enumerating[F[_], A](run: Int => (Int, F[A]))

object Enumerating {
  implicit def enumeratingFunctor[F[_] : Functor]: Functor[Enumerating[F, *]] = new Functor[Enumerating[F, *]] {
    override def fmap[A, B](f: A => B)(a: Enumerating[F, A]): Enumerating[F, B] = Enumerating { i =>
      val (i2, fa) = a.run(i)
      (i2, fa.map(f))
    }
  }

  implicit def enumeratingApply[F[_] : Apply]: Apply[Enumerating[F, *]] = new Apply[Enumerating[F, *]] {
    override def fmap[A, B](f: A => B)(a: Enumerating[F, A]): Enumerating[F, B] = enumeratingFunctor[F].fmap(f)(a)

    override def ap[A, B](ixf: Enumerating[F, A => B])(ixa: Enumerating[F, A]): Enumerating[F, B] = Enumerating[F, B] { i =>
      val (i1, ff) = ixf.run(i)
      val (i2, fa) = ixa.run(i1)
      (i2, ff.ap(fa))
    }
  }

  implicit def enumeratingPointed[F[_] : Pointed]: Pointed[Enumerating[F, *]] = new Pointed[Enumerating[F, *]] {
    override def fmap[A, B](f: A => B)(a: Enumerating[F, A]): Enumerating[F, B] = enumeratingFunctor[F].fmap(f)(a)

    override def pure[A](a: A): Enumerating[F, A] = Enumerating[F, A] { i => (i, Pointed[F].pure(a)) }
  }

  implicit def enumeratingApplicative[F[_] : Applicative]: Applicative[Enumerating[F, *]] = new Applicative[Enumerating[F, *]] {
    override def ap[A, B](ff: Enumerating[F, A => B])(fa: Enumerating[F, A]): Enumerating[F, B] = enumeratingApply[F].ap(ff)(fa)

    override def pure[A](a: A): Enumerating[F, A] = enumeratingPointed[F].pure(a)
  }
}
