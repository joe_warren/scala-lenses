package it.doscienceto.lens.typeclasses.instances

import it.doscienceto.lens.typeclasses.{Applicative, Apply}

/* Reverse the order of an applicative's effects */
case class Backwards[F[_], A](underlyingFunctor: F[A])

object Backwards {
  implicit def backwardsApplicative[F[_]](implicit app: Applicative[F]): Applicative[Backwards[F, *]] = new Applicative[Backwards[F, *]] {
    override def pure[A](a: A): Backwards[F, A] = Backwards[F, A](app.pure[A](a))

    override def ap[A, B](ff: Backwards[F, A => B])(fa: Backwards[F, A]): Backwards[F, B] = Backwards[F, B](
      app.fmap2[A, A => B, B] { case (a, f) => f(a) }(fa.underlyingFunctor, ff.underlyingFunctor)
    )
  }

  implicit def backwardsApply[F[_]](implicit app: Apply[F]): Apply[Backwards[F, *]] = new Apply[Backwards[F, *]] {
    override def ap[A, B](ff: Backwards[F, A => B])(fa: Backwards[F, A]): Backwards[F, B] = Backwards[F, B](
      app.fmap2[A, A => B, B] { case (a, f) => f(a) }(fa.underlyingFunctor, ff.underlyingFunctor)
    )

    override def fmap[A, B](f: A => B)(a: Backwards[F, A]): Backwards[F, B] =
      Backwards(app.fmap(f)(a.underlyingFunctor))
  }
}
