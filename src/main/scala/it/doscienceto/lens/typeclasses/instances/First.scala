package it.doscienceto.lens.typeclasses.instances

import it.doscienceto.lens.typeclasses.Semigroup

case class First[A](value: A)

object First {
  implicit def firstSemigroup[A]: Semigroup[First[A]] = (a: First[A], _: First[A]) => a
}