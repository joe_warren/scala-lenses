package it.doscienceto.lens.typeclasses.instances

import it.doscienceto.lens.typeclasses.Indexable

case class MappedIndexable[I, I2, A, B, P[_, _]](f: I2 => I, p: P[A, B])

object MappedIndexable {
  implicit def mappedIndexableIsIndexable[I, I2, P[_, _]](
      implicit wrapped: Indexable[I, P])
    : Indexable[I2, MappedIndexable[I, I2, *, *, P]] =
    new Indexable[I2, MappedIndexable[I, I2, *, *, P]] {
      override def indexed[A, B](p: MappedIndexable[I, I2, A, B, P])(i: I2)(
          a: A): B = wrapped.indexed(p.p)(p.f(i))(a)
    }
}
