package it.doscienceto.lens.typeclasses.instances
import it.doscienceto.lens.typeclasses.Monoid

case class Endo[A](f: A => A)
object Endo {
  implicit def endoMonoid[A]: Monoid[Endo[A]] = new Monoid[Endo[A]] {
    override def zero: Endo[A] = Endo(identity)

    override def accumulate(a: Endo[A], b: Endo[A]): Endo[A] = Endo { x => b.f(a.f(x)) }
  }
}
