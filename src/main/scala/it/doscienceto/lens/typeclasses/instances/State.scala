package it.doscienceto.lens.typeclasses.instances

import it.doscienceto.lens.typeclasses.Monad

case class State[S, A](fn: S => (A, S))
object State {
  implicit def stateMonad[S]: Monad[State[S, *]] = new Monad[State[S, *]] {
    override def bind[A, B](f: A => State[S, B])(fa: State[S, A]): State[S, B] = State { s => {
      val (a1, s1) = fa.fn(s)
      f(a1).fn(s1)
    }}

    override def pure[A](a: A): State[S, A] = State { s => (a, s) }
  }
}
