package it.doscienceto.lens.typeclasses.instances
import it.doscienceto.lens.typeclasses.Monoid

case class Sum(value: Int)
object Sum {
  implicit def sumMonoid: Monoid[Sum] = new Monoid[Sum] {
    override def zero: Sum = Sum(0)

    override def accumulate(as: Sum, bs: Sum): Sum = (as, bs) match {
      case (Sum(a), Sum(b)) => Sum(a + b)
    }
  }
}