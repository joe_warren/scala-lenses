package it.doscienceto.lens.typeclasses.instances

import it.doscienceto.lens.typeclasses.Profunctor

/* used to work with Isomorphism's */
case class Exchange[S, T, A, B](sa: S => A, bt: B => T)

object Exchange {
  implicit def exchangeIsProfunctor[A, B]: Profunctor[Exchange[*, *, A, B]] = new Profunctor[Exchange[*, *, A, B]] {
    override def dimap[S2, S1, T1, T2](f1: S2 => S1, f2: T1 => T2)(fa: Exchange[S1, T1, A, B]): Exchange[S2, T2, A, B] = Exchange[S2, T2, A, B](
      f1.andThen(fa.sa), f2.compose(fa.bt)
    )
  }
}
