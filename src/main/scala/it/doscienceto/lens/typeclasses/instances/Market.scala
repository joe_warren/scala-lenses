package it.doscienceto.lens.typeclasses.instances

import it.doscienceto.lens.typeclasses.Choice

case class Market[S, T, A, B](bt: B => T, seta: S => Either[T, A])

object Market {
  /* used to work with Prism */

  implicit def marketIsChoice[A, B]: Choice[Market[*, *, A, B]] = new Choice[Market[*, *, A, B]] {
    override def left[S, T, R](m: Market[S, T, A, B]): Market[Either[S, R], Either[T, R], A, B] = Market[Either[S, R], Either[T, R], A, B](
      { case b => Left(m.bt(b)) },
      { case Left(s) => m.seta(s) match {
        case Left(t) => Left(Left(t))
        case Right(a) => Right(a)
      }
      case Right(c) => Left(Right(c))
      }
    )

    override def right[S, T, L](m: Market[S, T, A, B]) : Market[Either[L, S], Either[L, T], A, B] = Market[Either[L, S], Either[L, T], A, B](
      { case b => Right(m.bt(b)) },
      { case Left(c) => Left(Left(c))
      case Right(s) => m.seta(s) match {
        case Left(t) => Left(Right(t))
        case Right(a) => Right(a)
      }
      }
    )

    override def dimap[S2, S1, T1, T2](f1: S2 => S1, f2: T1 => T2)(m: Market[S1, T1, A, B]): Market[S2, T2, A, B] = Market[S2, T2, A, B](
        m.bt.andThen(f2),
        { case s => m.seta(f1(s)) match {
          case Left(t1) => Left(f2(t1))
          case Right(b) => Right(b)
        }})
  }

}
