package it.doscienceto.lens.typeclasses.instances

import it.doscienceto.lens.typeclasses.{Monad, Monoid}
import it.doscienceto.lens.typeclasses.Syntax._
case class Writer[W, A](written: W, value: A)

object Writer {
  implicit def writerMonad[W: Monoid]: Monad[Writer[W, *]] = new Monad[Writer[W, *]] {
    override def bind[A, B](f: A => Writer[W, B])(fa: Writer[W, A]): Writer[W, B] = {
      val fb = f(fa.value)
      Writer[W, B](fb.written <> fa.written, fb.value)
    }

    override def pure[A](a: A): Writer[W, A] = Writer(Monoid[W].zero, a)
  }
}
