package it.doscienceto.lens.typeclasses.instances

import it.doscienceto.lens.typeclasses.{Applicative, Choice, Indexable}

case class IndexedFunction[I, A, B](run: (I, A) => B)

object IndexedFunction {

  implicit def indexedFunctionIsApplicative[I, Arg]: Applicative[IndexedFunction[I, Arg, *]] =
    new  Applicative[IndexedFunction[I, Arg, *]]{
      override def pure[A](a: A): IndexedFunction[I, Arg, A] = IndexedFunction[I, Arg, A]{(_:I, _:Arg) => a}

      override def ap[A, B](ff: IndexedFunction[I, Arg, A => B])(fa: IndexedFunction[I, Arg, A]): IndexedFunction[I, Arg, B] =
        IndexedFunction{ (i: I, arg: Arg) =>
          ff.run(i, arg)(fa.run(i, arg))
        }
    }

  implicit def indexedFunctionIsChoice[I]: Choice[IndexedFunction[I, *, *]] = new Choice[IndexedFunction[I, *, *]] {
    override def left[A, B, C](p: IndexedFunction[I, A, B]): IndexedFunction[I, Either[A, C], Either[B, C]] =
      IndexedFunction[I, Either[A, C], Either[B, C]]{ (i: I, eac: Either[A, C]) =>
        eac match{
          case Left(a) => Left(p.run(i, a))
          case Right(c) => Right(c)
        }
      }

    override def right[A, B, C](p: IndexedFunction[I, A, B]): IndexedFunction[I, Either[C, A], Either[C, B]] =
      IndexedFunction[I, Either[C, A], Either[C, B]]{ (i: I, eca: Either[C, A]) =>
        eca match {
          case Left(c) => Left(c)
          case Right(a) => Right(p.run(i, a))

        }
      }

    override def dimap[A, B, C, D](f1: A => B, f2: C => D)(fa: IndexedFunction[I, B, C]): IndexedFunction[I, A, D] =
      IndexedFunction[I, A, D]{ (i: I, a: A) =>
        f2( fa.run(i, f1(a)) )
      }
  }

  implicit def indexedFunctionIsIndexable[I, Arg]:
  Indexable[I, IndexedFunction[I, *, *]] =
    new Indexable[I, IndexedFunction[I, *, *]] {
      override def indexed[A, B](p: IndexedFunction[I, A, B])(i: I)(a: A): B = p.run(i, a)
    }
}
