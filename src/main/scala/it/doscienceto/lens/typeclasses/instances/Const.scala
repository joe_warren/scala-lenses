package it.doscienceto.lens.typeclasses.instances

import it.doscienceto.lens.typeclasses.{Applicative, Apply, Contravariant, Functor, Monoid, Semigroup}

case class Const[A, B](value: A)

object Const {
  implicit def constFunctor[C]: Functor[Const[C, *]] = new Functor[Const[C, *]] {
    override def fmap[A, B](f: A => B)(a: Const[C, A]): Const[C, B] = Const[C, B](a.value)
  }
  implicit def constContravariant[C]: Contravariant[Const[C, *]] = new Contravariant[Const[C, *]] {
    override def contramap[A, B](f: A => B)(b: Const[C, B]): Const[C, A] = Const[C, A](b.value)
  }

  implicit def constMonoidApplicative[C: Monoid]: Applicative[Const[C, *]] = new Applicative[Const[C, *]] {
    override def pure[A](a: A): Const[C, A] = Const[C, A](implicitly[Monoid[C]].zero)

    override def ap[A, B](ff: Const[C, A => B])(fa: Const[C, A]): Const[C, B] = Const[C, B](
      implicitly[Monoid[C]].accumulate(ff.value, fa.value)
    )
  }

  implicit def constSemigroupApply[C: Semigroup]: Apply[Const[C, *]] = new Apply[Const[C, *]] {
    override def ap[A, B](ff: Const[C, A => B])(fa: Const[C, A]): Const[C, B] = Const[C, B](
      implicitly[Semigroup[C]].accumulate(ff.value, fa.value)
    )

    override def fmap[A, B](f: A => B)(a: Const[C, A]): Const[C, B] = constFunctor.fmap(f)(a)
  }
}
