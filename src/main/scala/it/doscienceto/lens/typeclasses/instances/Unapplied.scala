package it.doscienceto.lens.typeclasses.instances

import it.doscienceto.lens.typeclasses.Semigroup

// Unlike Endo, this isn't lawful, because it relies on the argument passed to apply being associative
sealed trait Unapplied[A] {
  def apply(f: A => A => A): A
}

object Unapplied {
  def apply[A](a: A): Unapplied[A] = new Unapplied[A] {
    override def apply(f: A => A => A): A = a
  }

  implicit def unappliedSemigroup[A]: Semigroup[Unapplied[A]] = new Semigroup[Unapplied[A]] {
    override def accumulate(a: Unapplied[A], b: Unapplied[A]): Unapplied[A] = new Unapplied[A] {
      override def apply(f: A => A => A): A = f(a.apply(f))(b.apply(f))
    }
  }
}
