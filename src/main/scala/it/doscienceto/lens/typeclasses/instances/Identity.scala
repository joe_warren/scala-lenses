package it.doscienceto.lens.typeclasses.instances

import it.doscienceto.lens.typeclasses.Monad

case class Identity[A](value: A)

object Identity {
  implicit val identityMonad: Monad[Identity] = new Monad[Identity] {
    override def bind[A, B](f: A => Identity[B])(fa: Identity[A]): Identity[B] = f(fa.value)
    override def pure[A](a: A): Identity[A] = Identity(a)
  }
}
