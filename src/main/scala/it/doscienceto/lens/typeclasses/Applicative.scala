package it.doscienceto.lens.typeclasses

trait Applicative[F[_]] extends Pointed[F] with Apply[F] {
  override def fmap[A, B](f: A => B)(a: F[A]): F[B] = ap(pure(f))(a)
}

object Applicative {
  def apply[F[_]](implicit applicative: Applicative[F]): Applicative[F] = applicative
}

