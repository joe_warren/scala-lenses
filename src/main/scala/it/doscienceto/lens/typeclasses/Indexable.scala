package it.doscienceto.lens.typeclasses

trait Indexable[I, P[_, _]]{
  def indexed[A, B](p: P[A, B])(i: I)(a: A): B
}

trait IndexableSyntax {
  implicit class IndexableOps[I, A, B, P[_, _]: Indexable[I, *[_, _]]](instance: P[A, B]){
    def indexed(i: I)(a: A): B = Indexable[I, P].indexed(instance)(i)(a)
  }
}

object Indexable{
  def apply[I, P[_, _]](implicit indexable: Indexable[I, P]): Indexable[I, P] = indexable

  implicit def functionIsIndexable[I]: Indexable[I, Function1] =
    new Indexable[I, Function1] {
      override def indexed[A, B](p: A => B)(i: I)(a: A): B = p(a)
    }
}

