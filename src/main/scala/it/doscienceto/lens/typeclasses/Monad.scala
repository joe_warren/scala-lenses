package it.doscienceto.lens.typeclasses

trait Monad[F[_]] extends Applicative[F] {
  def bind[A, B](f: A => F[B])(fa: F[A]): F[B]
  override def ap[A, B](ff: F[A => B])(fa: F[A]): F[B] =
    bind[A, B](a => bind[A => B, B](f => pure(f(a)))(ff)) (fa)
}

trait MonadSyntax {
  implicit class MonadOps[F[_]: Monad, A](instance: F[A]){
    def flatMap[B](f: A => F[B]): F[B] = Monad[F].bind(f)(instance)
  }
}

object Monad {
  def apply[F[_]](implicit monad: Monad[F]): Monad[F] = monad

  implicit def EitherMonad[L]: Monad[Either[L, *]] = new Monad[Either[L, *]] {
    override def bind[A, B](f: A => Either[L, B])(fa: Either[L, A]): Either[L, B] = fa match {
      case Right(a) => f(a)
      case Left(l) => Left(l)
    }

    override def pure[A](a: A): Either[L, A] = Right(a)
  }
}

