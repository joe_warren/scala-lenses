package it.doscienceto.lens

import it.doscienceto.lens.typeclasses._
import it.doscienceto.lens.typeclasses.Syntax._
import it.doscienceto.lens.typeclasses.instances._

trait Traversal[S, T, A, B]{ that =>
  def applyTraversal[F[_] : Applicative](f: A => F[B]): (S => F[T])
  def apply[F[_] : Applicative](f: A => F[B]): (S => F[T]) = this.applyTraversal(f)

  def mapOf(f: A => B)(fa: S): T =  this.apply{ a => Identity(f(a))}.apply(fa).value
  def foldMapOf[M: Monoid](f: A => M)(s: S): M = this.apply{ a => Const[M, B](f(a))}.apply(s).value
  def foldOf[Acc](f: Acc => A => Acc)(z0: Acc)(s: S): Acc = this.foldMapOf[Endo[Acc]]{ a => Endo{z => f(z)(a)}}(s).f(z0)
  def buildListOf(s: S): List[A] = apply{
    a => Const[List[A], B](List(a))
  }.apply(s).value

  def headOptionOf(s: S): Option[A] = foldMapOf(Some(_):Option[A])(s)

  def reverse : Traversal[S, T, A, B] = new Traversal[S, T, A, B]{
    override def applyTraversal[F[_] : Applicative](f: A => F[B]): S => F[T] = {
       s: S => that.apply({ a:A => Backwards[F, B](f(a))}).apply(s).underlyingFunctor
    }
  }
  def enumerate: IndexedTraversal[Int, S, T, A, B] = new IndexedTraversal[Int, S, T, A, B] {
    override def applyIndexedTraversal[P[_, _]: Indexable[Int, *[_,_]], F[_]: Applicative]
    (p: P[A, F[B]]): S => F[T] = { s: S =>
      that.applyTraversal[Enumerating[F, *]]{ a =>
        Enumerating[F, B]{ i =>
          (i+1, p.indexed(i)(a))
        }
      }.apply(s).run(0)._2
    }
  }
}

object Traversal {
  implicit class MonomorphicTraversal[S, T, A](traversal: Traversal[S, T, A, A]){
    def taking(i: Int): Traversal[S, T, A, A] = traversal.enumerate.elements(_ < i)
    def dropping(i: Int): Traversal[S, T, A, A] = traversal.enumerate.elements(_ >= i)

    def headOptional: Optional[S, T, A, A] = Optional.optional({ s: S =>
      traversal.headOptionOf(s) match {
        case Some(a) => Right(a)
        case None => Left(traversal.mapOf(identity)(s))
      }},
      (a: A, s:S) => traversal.enumerate.elements(_==0).mapOf(_ => a)(s)
    )
  }
  def compose[A, B, S1, T1, S2, T2](l1: Traversal[S2, T2, S1, T1], l2: Traversal[S1, T1, A, B]): Traversal[S2, T2, A, B] =
    new Traversal[S2, T2, A, B]{
    override def applyTraversal[F[_] : Applicative](f: A => F[B]): S2 => F[T2] = l1(l2(f))
  }
}
