package it.doscienceto

package object lens {
  type Simple[Optic[_, _, _, _], S, A] = Optic[S, S, A, A]
  type SimpleNonEmptyTraversal[S, A] = Simple[NonEmptyTraversal, S, A]
  type SimpleTraversal[S, A] = Simple[Traversal, S, A]
  type SimpleLens[S, A] = Simple[Lens, S, A]
  type SimpleIsomorphism[S, A] = Simple[Isomorphism, S, A]
  type SimpleOptional[S, A] = Simple[Optional, S, A]
  type SimplePrism[S, A] = Simple[Prism, S, A]
  // or ...
}
