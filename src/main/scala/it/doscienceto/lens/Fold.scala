package it.doscienceto.lens

import it.doscienceto.lens.typeclasses.{Applicative, Monoid}
import it.doscienceto.lens.typeclasses._
import it.doscienceto.lens.typeclasses.Syntax._
import it.doscienceto.lens.typeclasses.instances._
trait Fold[S, A] {
  def applyFold[F[_]: Contravariant: Applicative](f: A=> F[A]): S => F[S]
  def apply[F[_]: Contravariant: Applicative](f: A=> F[A]): S => F[S] = applyFold(f)

  def foldMapOf[M: Monoid](f: A => M)(s: S): M = this.apply(a => Const[M, A](f(a))).apply(s).value
  def foldOf[Acc](f: Acc => A => Acc)(z0: Acc)(s: S): Acc = this.foldMapOf[Endo[Acc]](a => Endo { z => f(z)(a) })(s).f(z0)
  def buildListOf(s: S): List[A] = foldOf[List[A]](list => a => a+:list)(List.empty[A])(s)
}

object Fold {
  implicit def monomorphicTraversalsAreFold[S, A](tr: Traversal[S, S, A, A]): Fold[S, A] = new Fold[S, A]{
    override def applyFold[F[_]: Contravariant: Applicative](f: A => F[A]): S => F[S] = tr.apply(f)
  }
}
