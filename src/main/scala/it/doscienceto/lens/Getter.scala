package it.doscienceto.lens

import it.doscienceto.lens.typeclasses._
import it.doscienceto.lens.typeclasses.Syntax._
import it.doscienceto.lens.typeclasses.instances._

trait Getter[S, A]{
  def get(s: S): A
}

object Getter {
  implicit def monoidTraversalIsGetter[S, A: Monoid](t: Traversal[S, _, A, A]): Getter[S, A] = new Getter[S, A]{
    override def get(s: S): A = t.foldMapOf(identity)(s)
  }
  implicit def semigroupNETraversalIsGetter[S, A: Semigroup](t: NonEmptyTraversal[S, _, A, A]): Getter[S, A] = new Getter[S, A]{
    override def get(s: S): A = t.foldMapOf(identity)(s)
  }
  implicit def lensIsGetter[S, T, A, B](l: Lens[S, T, A, B]): Getter[S, A] = new Getter[S, A]{
    override def get(s: S): A = l.get(s)
  }
  def compose[S, M, A](g1: Getter[S, M], g2: Getter[M, A]): Getter[S, A] = new Getter[S, A]{
    override def get(s: S): A = g2.get(g1.get(s))
  }
}
