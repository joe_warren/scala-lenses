package it.doscienceto.lens

import it.doscienceto.lens.typeclasses._
import it.doscienceto.lens.typeclasses.Syntax._
import it.doscienceto.lens.typeclasses.instances._

/* A Prism can be thought of as a Traversal that can be turned around with re to obtain a Getter in the opposite direction.
   Alternatively, it can be thought of as a Isomorphism that can be partial in one direction.
 */
trait Prism[S, T, A, B] extends Optional[S, T, A, B] {
  def applyPrism[C[_, _]: Choice, F[_]: Pointed](s: C[A, F[B]]): C[S, F[T]]
  def apply[C[_, _]: Choice, F[_]: Pointed](s: C[A, F[B]]): C[S, F[T]] = applyPrism(s)

  def withPrism[R](f: (B=>T, S=> Either[T, A])=>R): R = this.apply[Market[*, *, A, B], Identity](
    Market[A, Identity[B], A, B](Identity(_), Right(_))) match {
    case m: Market[S, Identity[T], A, B] => f(
      m.bt.andThen(_.value),
      { case s => m.seta(s) match {
        case Left (Identity (t) ) => Left (t)
        case Right (a) => Right (a)
      }
      })
  }

  override def applyOptional[F[_] : Pointed](f: A => F[B]): (S => F[T]) = applyPrism(f)

  def re: Getter[B, T] = new Getter[B, T] {
    override def get(s: B): T = withPrism({case (bt,_) => bt(s)})
  }
}

object Prism {
  def compose[A, B, S1, T1, S2, T2](p1: Prism[S2, T2, S1, T1], p2: Prism[S1, T1, A, B]) = new Prism[S2, T2, A, B] {
    override def applyPrism[Ch[_, _] : Choice, F[_] : Pointed](s: Ch[A, F[B]]): Ch[S2, F[T2]] = p1(p2(s))
  }
  def prism[S, T, A, B](bt: B=> T, seta: S => Either[T, A]) = new Prism[S, T, A, B]{
    override def applyPrism[C[_, _] : Choice, F[_] : Pointed](choice: C[A, F[B]]): C[S, F[T]] = {
      Choice[C].dimap[S, Either[T, A], Either[T, F[B]], F[T]](seta, {
        case Left(t) => Pointed[F].pure(t)
        case Right(ft) => ft.map(bt)
      })( Choice[C].right(choice) )
    }
  }
  def simplePrism[S, A](as: A => S, sesa: S => Either[S, A]): SimplePrism[S, A] =
    prism[S, S, A, A](as, sesa)
  def simplerPrism[S, A](as: A => S, soa: S => Option[A]): SimplePrism[S, A] =
    simplePrism[S, A](as, s =>
    soa(s) match {
      case Some(a) => Right(a)
      case None => Left(s)
    }
  )
}
